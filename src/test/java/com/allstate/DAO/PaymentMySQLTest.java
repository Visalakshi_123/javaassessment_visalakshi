package com.allstate.DAO;

import com.allstate.DTO.Payment;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class PaymentMySQLTest {

    PaymentRepo paymentdao = new PaymentMySQL();

    @Test
    public void findByIdTest() {

        assertEquals(33, paymentdao.findById(3).getCustid());


    }

    @Test
    public void findByTypeTest() {

        assertEquals(2, paymentdao.findByType("Savings").size());


    }

    @Test
    public void saveTest() {
        Payment payment = new Payment(3, new Date(), "savings", 34.8, 23);
        assertEquals(1, paymentdao.create(payment));


    }

    @Test
    public void countTest() {
        assertEquals(0, paymentdao.count());
    }
}





