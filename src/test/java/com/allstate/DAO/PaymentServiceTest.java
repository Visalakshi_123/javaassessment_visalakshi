package com.allstate.DAO;

import com.allstate.DTO.Payment;
import com.allstate.Exceptions.OutOfRangeException;
import com.allstate.Service.PaymentService;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.isA;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PaymentServiceTest {
    PaymentService paymentService;
    @Test
    public void testFindByIDThrowsException(){

        Exception exception= assertThrows(OutOfRangeException.class,()->{
        Payment payment=paymentService.findById(0); });
        }

    @Test
    public void testFindByIdReturnsPayment() throws OutOfRangeException {
        Payment payment=paymentService.findById(0);
        assertThat(payment,isA(Payment.class));
    }


    @Test
    public void testFindByTypeThrowsException() throws OutOfRangeException {
        Exception exception= assertThrows(OutOfRangeException.class,()->{
            List<Payment> payment=paymentService.findByType(""); });
    }

    @Test
    public void testFindByTypeReturnsPayment() throws OutOfRangeException {
        List<Payment> payment=paymentService.findByType("Savings");
        assertThat(payment,instanceOf(Payment.class));
    }



}
