package com.allstate.DAO;

import com.allstate.DTO.Payment;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class PaymentMongoTest {

    PaymentRepo paymentdao = new PaymentMongo();


    @Test
    public void findByIdTest() {

        assertEquals(23, paymentdao.findById(3).getCustid());


    }

    @Test
    public void findByTypeTest() {

        assertEquals(24, paymentdao.findByType("Savings").size());


    }

    @Test
    public void saveTest() {
        Payment payment = new Payment(3, new Date(), "savings", 34.8, 23);
        assertEquals(0, paymentdao.create(payment));


    }

    @Test
    public void countTest(){
        assertEquals(25,paymentdao.count());
    }
}
