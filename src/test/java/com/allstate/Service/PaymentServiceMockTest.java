package com.allstate.Service;


import com.allstate.DAO.PaymentRepo;
import com.allstate.DTO.Payment;
import com.allstate.Exceptions.OutOfRangeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PaymentServiceMockTest {
    @Mock
    PaymentRepo paymentRepo;

    @Test
    public void testFindByddressThrowsException() {
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
        assertThrows(OutOfRangeException.class, () -> {
            List<Payment> payments = paymentService.findByType("");
        });

    }

    @Test
    public void testFindByIdPayment() throws OutOfRangeException {

       PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
       Payment payment=new  Payment(2, new Date(), "checking", 12.2, 3);
//        List<Payment> payments = new ArrayList<>();
//        payments.add(new Payment(2, new Date(), "checking", 12.2, 3));
//        payments.add(new Payment(21, new Date(), "checking", 123.2, 3));

        lenient().when(paymentRepo.findById(2)).thenReturn(payment);
        String type=paymentService.findById(2).getType();
       assertTrue(type.equals("checking"));
    }

    @Test
    public void testFindByIdthrowsException() throws OutOfRangeException {

        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
    Exception exception=assertThrows(OutOfRangeException.class,()->{
        Payment payment=paymentService.findById(0);
    });
    }

    @Test
    public void testFindByTypeReturnsPayments() throws OutOfRangeException {

        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
        List<Payment> payments = new ArrayList<>();
        payments.add(new Payment(2, new Date(), "checking", 12.2, 3));
        payments.add(new Payment(21, new Date(), "checking", 123.2, 3));

        lenient().when(paymentRepo.findByType("saving")).thenReturn(payments);
        assertTrue(paymentService.findByType("checking").size()==0);

    }

    @Test
    public void testSaveCheckforExistingRecord() throws OutOfRangeException {
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
        Payment payment=new  Payment(2, new Date(), "checking", 12.2, 3);

        lenient().when(paymentRepo.findById(3)).thenReturn(null);

        lenient().when(paymentRepo.create(payment)).thenReturn(Math.abs(anyInt()));
        assertTrue(paymentService.save(payment)>=0);
        verify(paymentRepo).create(payment);

    }

    @Test
    public void testSaveReturnsIntValue() throws OutOfRangeException{
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
        Payment payment=new  Payment(2, new Date(), "checking", 12.2, 3);
        int result=paymentService.save(payment);
        assertTrue(result!=-1);

    }


}

