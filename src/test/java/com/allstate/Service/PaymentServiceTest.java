package com.allstate.Service;

import com.allstate.DAO.PaymentRepo;
import com.allstate.DTO.Payment;
import com.allstate.Exceptions.OutOfRangeException;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.isA;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PaymentServiceTest {
    PaymentService paymentService;
    PaymentRepo paymentRepo;
    @Test
    public void testFindByIDThrowsException(){

        Exception exception= assertThrows(OutOfRangeException.class,()->{
        Payment payment=paymentService.findById(0); });
        }

    @Test
    public void testFindByIdReturnsPayment() throws OutOfRangeException {
        Payment payment=paymentService.findById(0);
        assertThat(payment,isA(Payment.class));
    }


    @Test
    public void testFindByTypeThrowsException() throws OutOfRangeException {
        Exception exception= assertThrows(OutOfRangeException.class,()->{
            List<Payment> payment=paymentService.findByType(""); });
    }

    @Test
    public void testFindByTypeReturnsPayment() throws OutOfRangeException {
        List<Payment> payment=paymentService.findByType("Savings");
        assertThat(payment,instanceOf(Payment.class));
    }


    @Test
    public void testSaveReturnsaInteger() throws OutOfRangeException {
        int payresult=paymentService.save(new Payment(5,new Date(),"Checking",12.4,23));
        assertTrue(payresult!=-2);
    }

    @Test
    public void testFindByddressThrowsException(){

        PaymentService paymentService =new PaymentServiceImpl(paymentRepo);
        assertThrows(OutOfRangeException.class,()->{
            List<Payment> payments=paymentService.findByType("");
        });

    }


}
