package com.allstate.DAO;

import com.allstate.DTO.Payment;

import java.util.List;

public interface PaymentRepo {

    public int create(Payment payment);

    public Payment findById(int id);

    public List<Payment> findByType(String type);

    public int count();


}