package com.allstate.DAO;

import com.allstate.DTO.Payment;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PaymentMySQL implements PaymentRepo {
    Connection connection;

    public PaymentMySQL() {
        this.connection = getConnection();
    }

    private Connection getConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/training?serverTimezone=UTC&useSSL=false", "root", "c0nygre");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return this.connection;

    }

    public boolean isConnected() {
        Statement statement = null;
        try {

            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from Payment");
            rs.first();
            System.out.println(rs.getString("name"));
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;

        }
    }

    @Override
    public int create(Payment payment) {
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "Insert into Payment(paymentdate,type,amount,custid) values(?,?,?,?)"

            );
            preparedStatement.setDate(1, new java.sql.Date(payment.getPaymentdate().getTime()));
            preparedStatement.setString(2, payment.getType());
            preparedStatement.setDouble(3, payment.getAmount());
            preparedStatement.setInt(4, payment.getCustid());
            return preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return -1;
        }
    }

    @Override
    public Payment findById(int id) {
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement("select * from Payment where id=?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.first();
            Payment payment = new Payment(
                    resultSet.getInt("id"),
                    resultSet.getDate("paymentdate"),
                    resultSet.getString("type"),
                    resultSet.getDouble("amount"),
                    resultSet.getInt("custid"));


            return payment;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Payment> findByType(String type) {
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement("select * from Payment where type=?");
            preparedStatement.setString(1, type);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Payment> payments = new ArrayList<>();
            while (resultSet.next()) {
                Payment payment = new Payment(
                        resultSet.getInt("id"),
                        resultSet.getDate("paymentdate"),
                        resultSet.getString("type"),
                        resultSet.getDouble("amount"),
                        resultSet.getInt("custid"));
                payments.add(payment);
            }

            return payments;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }


    @Override
    public int count() {
        Statement statement = null;
        try {

            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select count(*) from Payment");
            rs.first();
            int count = rs.getInt(1);
            return count;
        } catch (SQLException throwables) {
            throwables.printStackTrace();

            return 0;
        }
    }
}
