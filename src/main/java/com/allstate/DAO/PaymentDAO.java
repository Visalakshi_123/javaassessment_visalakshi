package com.allstate.DAO;

import com.allstate.DTO.Payment;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;


import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.*;


public class PaymentDAO {
    MongoClient mongoclient = new MongoClient("localhost", 27017);
    MongoDatabase database = mongoclient.getDatabase("training");
    MongoCollection<Document> collection = database.getCollection("payment");

    Payment payment = new Payment();

    public long save(Payment payment) {


        Document payment1 = new Document("id", payment.getId())
                .append("paymentdate", payment.getPaymentdate())
                .append("type", payment.getType())
                .append("custid", payment.getCustid())
                .append("amount", payment.getAmount());

        collection.insertOne(payment1);
        return collection.count();

    }


    public Payment findById(int id) {

        Document dpayment1 = collection.find(eq("id", id)).first();
        payment.setType(dpayment1.getString("type"));
        payment.setId(dpayment1.getInteger("id"));
        payment.setCustid(dpayment1.getInteger("custid"));
        payment.setAmount(dpayment1.getDouble("amount"));
        payment.setPaymentdate(dpayment1.getDate("paymentdate"));
        System.out.println(dpayment1.toJson());
        return payment;
    }

    public List<Payment> findByType(String type) {
        List<Payment> paymentList = new ArrayList<Payment>();
        MongoCursor<Document> cursor = collection.find(eq("type", type)).iterator();
        try {
            while (cursor.hasNext()) {
                Document dpayment1 = cursor.next();
                payment.setType(dpayment1.getString("type"));
                payment.setId(dpayment1.getInteger("id"));
                payment.setCustid(dpayment1.getInteger("custid"));
                payment.setAmount(dpayment1.getInteger("amount"));
                payment.setPaymentdate(dpayment1.getDate("paymentdate"));
                paymentList.add(payment);
            }
            return paymentList;
        } finally {
            cursor.close();
        }
    }

}