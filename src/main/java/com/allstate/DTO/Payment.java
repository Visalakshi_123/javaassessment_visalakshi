package com.allstate.DTO;

import java.util.Date;

public class Payment {
    //@BsonProperty(value="id")
    private int id;
    private Date paymentdate;
    private String type;
    private double amount;
    private int custid;

    public int getId() {
        return id;
    }

    public Payment() {

    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", paymentdate=" + paymentdate +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", custid=" + custid +
                '}';
    }

    public Payment(int id, Date paymentdate, String type, double amount, int custid) {
        this.id = id;
        this.paymentdate = paymentdate;
        this.type = type;
        this.amount = amount;
        this.custid = custid;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentdate() {
        return paymentdate;
    }

    public void setPaymentdate(Date paymentdate) {
        this.paymentdate = paymentdate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustid() {
        return custid;
    }

    public void setCustid(int custid) {
        this.custid = custid;
    }


}
