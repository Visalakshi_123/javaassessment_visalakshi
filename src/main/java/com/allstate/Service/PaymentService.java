package com.allstate.Service;

import com.allstate.DTO.Payment;
import com.allstate.Exceptions.OutOfRangeException;

import java.util.List;

public interface PaymentService {


    public int save(Payment payment) throws OutOfRangeException;

    public Payment findById(int id) throws OutOfRangeException;

    public List<Payment> findByType(String type) throws OutOfRangeException;

    public int count();
    }
