package com.allstate.Service;

import com.allstate.DAO.PaymentRepo;
import com.allstate.DTO.Payment;
import com.allstate.Exceptions.OutOfRangeException;

import java.util.List;

public class PaymentServiceImpl implements PaymentService {
    PaymentRepo paymentRepo;


    public PaymentServiceImpl(PaymentRepo paymentRepo) {
        this.paymentRepo = paymentRepo;

    }

    @Override
    public int save(Payment payment) throws OutOfRangeException {


        if (payment.getId() < 1) {
            throw new OutOfRangeException("id must be over 0", 1);
        }

        if (paymentRepo.findById(payment.getId()) == null)
            return paymentRepo.create(payment);
        else return -2;
    }


    @Override
    public Payment findById(int id) throws OutOfRangeException {


        if (id < 1) {
            throw new OutOfRangeException("id must be over 0", 1);
        }
        return paymentRepo.findById(id);
    }

    @Override
    public List<Payment> findByType(String type) throws OutOfRangeException {

        if (type == "") {
            throw new OutOfRangeException("Address can't be left blank", 1);
        }
        return paymentRepo.findByType(type);
    }

    @Override
    public int count() {
        return 0;
    }
}
